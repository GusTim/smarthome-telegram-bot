#!/usr/bin/env python
# coding=utf-8

import RPi.GPIO as GPIO                           
import sys, traceback                               

import picamera                               
import datetime as dt                             
import time
import random
import datetime
import telepot
import os
import numpy as np
import pickle
import cv2

from itertools import groupby
from collections import Counter
from telepot.loop import MessageLoop
from   datetime import timedelta
from   time import sleep                       


periodBetweenEvents = 120                           
NfileName ="_"
NameList = []
commandTerminal ="/home/pi/other"
chat_id = 000000000

try:
    saveEvent = False                                                                 
    # ����������, �������������� �� ������� ������� ������������ ������� � �������� ����
    lastAction = dt.datetime.now()                                                      
    lastAction = lastAction - timedelta(seconds=(periodBetweenEvents-10))          
    startEvent =  dt.datetime.now()

# �������-���������� ������������ ������� �������� =================================================================================
    def detect_motion(pin):                                                             
        global saveEvent                                                             
        global camera                                                                   
        global lastAction                                                              
        global startEvent                                                                
        print("��������� ���� " + str(pin) + " ���������� " + str(GPIO.input(pin)))
        GPIO.output(pinLED, GPIO.input(pin))                                            
        print((dt.datetime.now()-lastAction).seconds)
        if not saveEvent and ((dt.datetime.now()-lastAction).seconds > periodBetweenEvents): 
            if GPIO.input(pin):                                                     
                print("���������� ������ 10 ������ ����� ����������� �������")
                startEvent =  dt.datetime.now()                                    
                saveEvent = True                                                    

# ��� ���� ���������� =========================================================================================================================================
    def handle(msg):
	global chat_id
	chat_id = msg['chat']['id']

	msg_type, chat_type, chat_id = telepot.glance(msg)
	print(str(chat_id))	
	if msg_type == 'text':
            command = msg['text'].split()
            print 'Got command: %s' % command

	    if command[0] == '/add':
		commandTerminal = "mkdir images/"+str(command[1])
		os.system(commandTerminal)
		bot.sendMessage(chat_id, '����� '+str(command[1])+' �������, ������� ���� ����')
	    elif command[0] == '/delete':     
		commandTerminal = " rm -r images/"+str(command[1])
		os.system(commandTerminal)
		bot.sendMessage(chat_id, '����� '+str(command[1])+' �������')
	    elif command[0] == '/add_photo':
		global commandTerminal
		commandTerminal = "images/"+str(command[1])
		bot.sendMessage(chat_id, '����� '+str(command[1])+' ������� , ������� ���� ����')
	    elif command[0] == '/time':     
		bot.sendMessage(chat_id, str(datetime.datetime.now()))
	    elif command[0] == '/photo_face':
		print('Sending photo to ' + str(chat_id))
		bot.sendMessage(chat_id,NfileName)
		bot.sendPhoto(chat_id, photo = open('./face_detection.png', 'rb'))
	    elif command[0] == '/video':
		print('Sending video to ' + str(chat_id))
		bot.sendMessage(chat_id,NfileName)
		bot.sendVideo(chat_id, video = open(NfileName, 'rb'))
	    elif command[0] == '/photo':
		print('Capturing photo...')
		print('Sending photo to ' + str(chat_id))
		camera.capture('/home/pi/image.jpg')
		bot.sendMessage(chat_id,'�������!')
		bot.sendPhoto(chat_id, photo = open('./image.jpg', 'rb')) 
	else:

	    if "reply_to_message" in msg and "/crop" in msg["text"]: 
		msg = msg["reply_to_message"]
		msg_type, chat_type, chat_id = telepot.glance(msg)
	    elif not msg_type in ["document", "photo"]:
		return


	    if msg_type == "document": 
		if msg["document"]["mime_type"] in ["image/png", "image/jpeg"]: 
		    file_id = msg["document"]["file_id"]
		else:
		    return 
	    elif msg_type == "photo": 
		file_id = msg["photo"][-1]["file_id"] 
	    else:
		return
            
            os.system("cd images/")

	    file_path = commandTerminal+"/" + bot.getFile(file_id)["file_path"].split("/")[1] 
	    bot.download_file(file_id, file_path )
 
            os.system("cd ..")


	    print('Save photo...')



# ��� ������������� ���� =========================================================================================================================  
    def faces(VideoName):
        face_cascade = cv2.CascadeClassifier('cascades/data/haarcascade_frontalface_alt2.xml')

        recognizer = cv2.face.LBPHFaceRecognizer_create()
        recognizer.read("./recognizers/face-trainner.yml")

        labels = {"person_name": 1}
        with open("pickles/face-labels.pickle", 'rb') as f:
                og_labels = pickle.load(f)
                labels = {v:k for k,v in og_labels.items()}

        cap = cv2.VideoCapture(VideoName)
	global NameList
	NameList = []

	while(True):
	    # Capture frame-by-frame
	    ret, frame = cap.read()

	    try:
		gray  = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	    except Exception:
		break
	    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.5, minNeighbors=5)
	    for (x, y, w, h) in faces:
		#print(x,y,w,h)
		roi_gray = gray[y:y+h, x:x+w] #(ycord_start, ycord_end)
		roi_color = frame[y:y+h, x:x+w]
    
		# recognize? deep learned model predict keras tensorflow pytorch scikit learn
		id_, conf = recognizer.predict(roi_gray)
		if conf>=80 :
			#print(5: #id_)
			print(labels[id_])
			NameList.append(labels[id_])
			font = cv2.FONT_HERSHEY_SIMPLEX
			name = labels[id_]
			color = (255, 255, 255)
			stroke = 2
			cv2.putText(frame, name, (x,y), font, 1, color, stroke, cv2.LINE_AA)

		img_item = "face_detection.png"
		cv2.imwrite(img_item, frame[y:y+h+100, x:x+w+100])
	    
		color = (255, 0, 0) #BGR 0-255 
		stroke = 2
		end_cord_x = x + w
		end_cord_y = y + h
		cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), color, stroke)
		
	    	if cv2.waitKey(1) & 0xFF == ord('q'):
		    break
		# When everything done, release the capture
        cap.release()
        cv2.destroyAllWindows()

# �������� ��� ��������� =========================================================================================================================================
    camera = picamera.PiCamera()                                        
    camera.rotation=180                                                 
    camera.resolution = (640,480)
    
    command = "windscribe connect"
    os.system(command)
  
    # ������������� �����
    GPIO.setmode(GPIO.BCM)

    pinLED=24                                                           
    pinSensor=15                                                        
    GPIO.setup(pinLED, GPIO.OUT, initial=0)                            
    GPIO.setup(pinSensor, GPIO.IN)                                     

    GPIO.add_event_detect(pinSensor, GPIO.BOTH, callback=detect_motion)

    camera.annotate_text_size = 25                                      

    stream = picamera.PiCameraCircularIO(camera, seconds=30)           
    camera.start_recording(stream, format='h264')                       
    bot = telepot.Bot('')

    MessageLoop(bot, handle).run_as_thread()
    print 'I am listening ...'
    cap = cv2.VideoCapture(0)

    fileNum = 1                                                     
    while 1:                                                           
        annotate_text = ""
        if saveEvent:
            annotate_text = "After event - "                            
        else:
            annotate_text = "Before event - "                           

        camera.annotate_text = annotate_text + dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')  
        if saveEvent and ((dt.datetime.now()-startEvent).seconds > 10):
            print("������ ������� ���������, ���������� � ���������")

            camera.stop_recording()                                     
	    fileName = '%03d' % fileNum + startEvent.strftime('_%Y_%m_%d_%H_%M_%S')+'.h264' # ��������� ��� �����
            print('����������� ������� ����� ��������� � ���� '+ fileName)
            stream.copy_to("/home/pi/"+fileName, seconds=15)            
            print('SAVE')
            stream.clear()                                              

            camera.start_recording(stream, format='h264')               
            fileNum+=1                                                 
       
            lastAction = dt.datetime.now()                              
            saveEvent = False                                           

	    #command = "MP4Box -add "+fileName+" "+fileName+".mp4"
	    command = "avconv -i "+fileName+" -r 25 -vcodec copy "+fileName+".mp4"
	    os.system(command)
	    os.remove(fileName)
	    print("conversion and delete")
	    NfileName="./"+fileName+".mp4"

	    faces(NfileName)
	    Frend = False

	   # NewNameList =  [el for el, _ in groupby(NameList)]
	    NewNameList = (list(set(NameList)))
	    c = Counter(NameList)
   	    NameListCount = len(NameList)

	    print('NameList : '+str(NameList))
            print('NewNameList : '+str(NewNameList))
            print('c : '+ str(c))
            print('NameListCount :'+str( NameListCount))
            print(' ')
            print(' ')
            print(' ')
	    max_c = 0
	    i_max_c =0
            lines =0

	    for i in NewNameList:
		print( str(i) +" : "+str(c[i]))
            	if c[i] >= max_c:
	    	    max_c=c[i]
		    i_max_c=i

	    print('max_c =  '+str(max_c))
            print('i_max_c =  '+str(i_max_c))
            print('max_c/NameListCount =  '+str(int(max_c)-int(NameListCount)))

	    if int(max_c)>=int(NameListCount*0.5):
                    bot.sendMessage(chat_id,'���������� �������� ��� ���(�) '+str(i_max_c))
                    Frend = True

	    if Frend == False:
		bot.sendMessage(chat_id,'���������� �������� ���������� ')
	        fileName = str(i_max_c)+".txt"
       	        f = open(fileName,'a+')
           	for line in f:
               	    lines+=1
            	    f.write(str(1+lines)+" "+str(max_c)+" "+str(NameListCount)+" \n")
            	    f.close()

            bot.sendPhoto(chat_id, photo = open('./face_detection.png', 'rb'))
	    bot.sendVideo(chat_id, video = open(NfileName, 'rb'))
        else:
            camera.wait_recording(0.2)                                  
	
except KeyboardInterrupt:
    # ...
    print("Exit pressed Ctrl+C")                                  
except:
    # ...
    print("Other Exception")                                            
    print("--- Start Exception Data:")
    traceback.print_exc(limit=2, file=sys.stdout)                      
    print("--- End Exception Data:")
finally:
    command = "windscribe disconnect"
    os.system(command)

    camera.close()                                                      
    print("CleanUp")                                                    
    GPIO.cleanup()   
